
let btn_insert = document.getElementById("insert");
let btn_serch = document.getElementById("serch");
let btn_delete = document.getElementById("delete");
let btn_update = document.getElementById("update");
btn_insert.addEventListener("click", function(){
    
    insert_person().then((res)=>{
        console.log(res);
    }).catch((err)=>{
        console.log(err);
    })
})
btn_serch.addEventListener("click", function(){
    let cc = document.getElementById("cc").value;    
    show_person(cc).then((res)=>{
        console.log(res);
    }).catch((err)=>{
        console.log(err);
    })
})
btn_delete.addEventListener("click",function(){
    let cc = document.getElementById("cc").value;
    delete_person(cc).then((res)=>{
        console.log(res);
    }).catch((err)=>{
        console.log(err);
    })
})
btn_update.addEventListener("click", function(){
    let cc = document.getElementById("cc").value;
    update_person(cc).then((res)=>{
        console.log(res);
    }).catch((err)=>{
        console.log(err);
    })
})
