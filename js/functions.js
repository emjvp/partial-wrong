


let insert_person = ()=>{
    
    return new Promise((res, rej)=>{    
            let count = localStorage.length;
            let gender = "Other";
            
            if(document.getElementById("gender").checked)
            {
                gender = document.getElementById("gender").value;
            }else if(document.getElementById("gender2").checked)
            {
                gender = document.getElementById("gender2").value;
            }else if(document.getElementById("gender3").checked)
            {
                gender = document.getElementById("gender3").value;
            }            
            let Person = {
                "name": document.getElementById("name").value,
                "gender": gender,
                "cc":document.getElementById("cc").value,
                "age":document.getElementById("age").value
            }              
            localStorage.setItem(`Person${count}`, JSON.stringify(Person));            
            if(localStorage.getItem(`Person${count}`)){
                res(localStorage.getItem(`Person${count}`));
                count++;
            }else{
                rej("Error");
            }
    })
}
let show_person = (cc) =>{
    
    return new Promise((res, rej)=>{
        
        let years_for_juv;
        let obj;
        let valueFound = false;
        for(let i = 0; i<localStorage.length;i++)
        {   
            obj = JSON.parse(localStorage.getItem(`Person${i}`));                        
            if(obj['cc'] === cc)
            {
                
                if(obj['gender'] == "M"){
                    years_for_juv = 67 - parseInt(obj['age']);    
                }else if(obj['gender'] == "F") {
                    
                    years_for_juv = 57 - parseInt(obj['age']);    
                }
                if(years_for_juv < 0){
                    years_for_juv = "jubilado";
                }
                valueFound = true;
                break;
            }            
        }
        if(valueFound)
        {
            document.getElementById("results").style.display = "block";
            document.getElementById("name_r").value = obj['name'];
            document.getElementById("gender_r").value = obj['gender'];
            document.getElementById("cc_r").value = obj['cc'];
            document.getElementById("years_for_juv").value = years_for_juv;
            res(`the years for the retairement of ${obj['name']} are ${years_for_juv}`);
        }else{
            rej("data not founded");
        }
    })
}
let delete_person = (cc)=>{
    return new Promise((res, rej)=>{
        
        let start_lenght = localStorage.length;
        let value_deleted = undefined;
        let end_lenght = 0;
        for(let i; i < localStorage.length;i++)
        {   
            obj = JSON.parse(localStorage.getItem(`Person${i}`));             
            if(obj['cc'] === cc)
            {
                value_deleted = obj;
                localStorage.removeItem(`Person${i}`);
                end_lenght = start_lenght-1;
            }            
        }
        if(end_lenght < start_lenght){
            res(value_deleted);
        }else
        {
            rej("Error with the elimination");
        }
    })
}
let update_person = (cc)=>{    
    return new Promise((res, rej)=>{  
        let i=0
        let obj;  
        let obj_after;        
        for(i = 0; i < localStorage.length; i++){
            obj = JSON.parse(localStorage.getItem(`Person${i}`));
            if(obj['cc'] === cc)
            {
                obj_after = obj;
                break;   
            }
        }
        if(document.getElementById("gender").checked)
        {
            gender = document.getElementById("gender").value;
        }else if(document.getElementById("gender2").checked)
        {
            gender = document.getElementById("gender2").value;
        }else if(document.getElementById("gender3").checked)
        {
            gender = document.getElementById("gender3").value;
        }            
        let Person = {
            "name": document.getElementById("name").value,
            "gender": gender,
            "cc":document.getElementById("cc").value,
            "age":document.getElementById("age").value
        }
        if(obj_after == Person){
            rej("There was an error with the updating")    
        }        
        else{
            localStorage.setItem(`Person${i}`,JSON.stringify(Person));
            alert("The data has been updated succesfully")
            res(localStorage.getItem(`Person${i}`));
        }                
    })
}